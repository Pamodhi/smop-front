
export const environment = {
  production: false,
  apiUrl: 'http://localhost:8080/SMoP',
  rootUrl: 'http://localhost:4200/',


  userPermittion: [
    {
      role: '1',
      nav: [
        'home',
        'income',
        'expenses',
        'bill',
        'accounts',
        'payees',
        'future-goals',
        'family-sync',
        'reports',
        'help'
      ],
      hidden: true
    },
    {
      role: '2',
      nav: [
        'home', 'pending-request',
      ],
      hidden: true
    },

  ],
};
