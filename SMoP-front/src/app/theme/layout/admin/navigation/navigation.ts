import { Injectable } from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

const NavigationItems = [
  {
    id: 'dashboard',
    title: 'Dashboard',
    type: 'group',
    icon: 'feather icon-monitor',
    children: [
      {
        id: 'home',
        title: 'home',
        type: 'item',
        url: '/dashboard/home',
        icon: 'feather icon-home'
      },
      {
        id: 'transaction',
        title: 'Transaction',
        type: 'collapse',
        icon: 'feather icon-sliders',
        children: [
          {
            id: 'income',
            title: 'Income',
            type: 'item',
            url: '/dashboard/transaction/income',
            target: false,
            breadcrumbs: false
          },
          {
            id: 'expenses',
            title: 'Expenses',
            type: 'item',
            url: '/dashboard/transaction/expenses',
            target: false,
            breadcrumbs: false
          },
          {
            id: 'bill',
            title: 'Bill',
            type: 'item',
            url: '/dashboard/transaction/bill',
            target: false,
            breadcrumbs: false
          }
        ]
      },
      {
        id: 'accounts',
        title: 'Accounts',
        type: 'item',
        url: '/dashboard/accounts',
        icon: 'feather icon-home'
      },
      {
        id: 'payees',
        title: 'Payees',
        type: 'item',
        url: '/dashboard/payees',
        icon: 'feather icon-home'
      },
      {
        id: 'future-goals',
        title: 'Future Goals',
        type: 'item',
        url: '/dashboard/future-goals',
        icon: 'feather icon-home'
      },
      {
        id: 'family-sync',
        title: 'Family Sync',
        type: 'item',
        url: '/dashboard/family-sync',
        icon: 'feather icon-home'
      },
      {
        id: 'reports',
        title: 'Reports',
        type: 'item',
        url: '/dashboard/reports',
        icon: 'feather icon-home'
      },
      {
        id: 'help',
        title: 'Help',
        type: 'item',
        url: '/dashboard/help',
        icon: 'feather icon-home'
      },
      {
        id: 'pending-request',
        title: 'Pending Request',
        type: 'item',
        url: '/dashboard/pending-request',
        icon: 'feather icon-home'
      },
    ]
  },
];

@Injectable()
export class NavigationItem {
  public get() {
    return NavigationItems;
  }
}
