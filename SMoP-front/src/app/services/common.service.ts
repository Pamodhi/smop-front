import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }
  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }
  getPermission(userId: string) {
    return this.http.get(environment.apiUrl + "/DashBoadManager/getUserType?userId=" + userId, this.config)
  }
}
