import { NgModule } from '@angular/core';
import { SharedModule } from '../theme/shared/shared.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';



@NgModule({
  declarations: [RegisterComponent, LoginComponent, ResetPasswordComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ]
})
export class AuthenticationModule { }
