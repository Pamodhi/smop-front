import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { trim } from "jquery";
import { Login } from "../../models/login.model";
import { AuthService } from "../../services/auth.service"


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  loginData;
  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.formBuilder();
    localStorage.clear();
  }

  formBuilder() {
    this.userForm = new FormGroup({
      userEmail: new FormControl("",[Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      password: new FormControl("", Validators.required),

    });
  }

  login() {
    if (this.userForm.controls.userEmail.valid) {
      if(this.userForm.controls.password.valid){
        this.authService.userLogin(this.userForm.value).subscribe(
          (data: Login) => {
            console.log(data);
            if (data.response) {
              localStorage.setItem('id',data.userId);
              localStorage.setItem('token', data.token);
              localStorage.setItem('accessType', data.accessType);

              if(data.accessType ='1'){
                return this.router.navigateByUrl('dashboard/home');
              }else if(data.accessType ='2'){
                return this.router.navigateByUrl('admin-dashboad/pending-request');
              }
            } else if(data.notverify){
              alert(data.notverify);
            }else{
              alert(data.error);
            }
          },
          (err) => {

          }
        );
      }else{
        this.userForm.controls.password.markAllAsTouched();
      }

    } else {
      this.userForm.controls.userEmail.markAllAsTouched();
    }

  }

}
