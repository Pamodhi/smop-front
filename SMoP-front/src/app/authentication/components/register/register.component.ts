import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { data, trim } from "jquery";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  selectedFiles: FileList;
  currentFile: File;
  step1: boolean = true;
  step2: boolean = false;
  step3: boolean = false;

  registerForm: FormGroup;
  verifyForm: FormGroup;
  verifyNumberValidate = false;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.formBuilder();
  }

  formBuilder() {
    this.registerForm = new FormGroup({
      fname: new FormControl("", Validators.required),
      lname: new FormControl("", Validators.required),
      nic: new FormControl("", [Validators.required, Validators.pattern("^([0-9]{9})(X|V)$|^([0-9]{11})")]),
      email: new FormControl("", [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      password: new FormControl("", Validators.required),
      repassword: new FormControl("", Validators.required)

    });

    this.verifyForm = new FormGroup({
      key1: new FormControl("", [Validators.required, Validators.maxLength(1)]),
      key2: new FormControl("", [Validators.required, Validators.maxLength(1)]),
      key3: new FormControl("", [Validators.required, Validators.maxLength(1)]),
      key4: new FormControl("", [Validators.required, Validators.maxLength(1)]),
    });
    this.verifyForm.controls.key1.markAllAsTouched();

  }

  verifyNumbers() {
    const key1 = trim(this.verifyForm.get('key1').value);
    const key2 = trim(this.verifyForm.get('key2').value);
    const key3 = trim(this.verifyForm.get('key3').value);
    const key4 = trim(this.verifyForm.get('key4').value);


    if (key1 !== '' && key1.length === 1) {
      if (key2 !== '' && key2.length === 1) {
        if (key3 !== '' && key3.length === 1) {
          if (key4 !== '' && key4.length === 1) {
            const velidateKey = key1 + key2 + key3 + key4;
            this.verifyNumberValidate = true;
            this.authService.verifyCode(velidateKey).subscribe(
              data => {
                if (data['msg'] == true) {
                  return this.router.navigateByUrl('dashboard/home');
                } else {
                  alert('verificaction code not valid');
                }
              },
              err => {
                console.log(err);

              }
            );
          } else {
            this.verifyForm.controls.key4.markAsTouched();
          }
        } else {
          this.verifyForm.controls.key3.markAsTouched();
        }
      } else {
        this.verifyForm.controls.key2.markAsTouched();
      }
    } else {
      this.verifyForm.controls.key1.markAsTouched();
    }
  }
  userExist() {
    this.authService.checkUserExist(this.registerForm.get('nic').value).subscribe(
      (data) => {
        if (data['msg'] == true) {
          alert('User Already Exist')
          this.registerForm.controls.nic.setValue('');
        }
      }
    );
  }

  emailExist() {
    this.authService.checkEmailExist(this.registerForm.get('email').value).subscribe(
      (data) => {
        if (data['msg'] == true) {
          alert('Email Already Exist')
          this.registerForm.controls.email.setValue('');
        }
      }
    );
  }

  checkPassword() {
    if (
      trim(this.registerForm.get('password').value) !== '' && this.registerForm.get('password').valid &&
      trim(this.registerForm.get('repassword').value) !== '' && this.registerForm.get('repassword').valid
    ) {
      if (this.registerForm.get('password').value === this.registerForm.get('repassword').value) {
        return true;
      } else {
        alert("Password dose not matched !")
        this.registerForm.get('repassword').value == "";
        this.registerForm.controls.repassword.markAllAsTouched();
        return false;
      }
    } else {
      alert("Password canot beempty !")
    }

  }

  createUser() {
    if (this.validateForm()) {
      if(this.checkPassword()){
        this.authService.createUser(this.registerForm.value).subscribe(
          (data) => {
            if (data['msg'] == 'Success') {
              localStorage.setItem('id', data['userId']);
              alert("Email send! Please verify user Details");
              this.step1 = false;
              this.step2 = false;
              this.step3 = true;
            } else if (data['msg'] == 'Error') {
              alert("Error occur during the process!");
              return this.router.navigateByUrl('');
            }
          },
          (err) => {
            alert(err);
            return this.router.navigateByUrl('');
          }
        );
      }else{
        alert("Password dose not matched ! ");
      }

    } else {
      this.registerForm.markAllAsTouched();
    }
  }

  varifyUser() {
    if (this.verifyNumberValidate) {
      return this.router.navigateByUrl('dashboard/home');
    } else {
      alert('verificaction code not valid')
    }
  }

  stepper(step: string) {
    console.log(step);

    if (step === "step1") {
        this.step1 = true;
        this.step2 = false;
        this.step3 = false;
    }
    if (step === "step2") {
      if(trim(this.registerForm.get('nic').value) !== '' && this.registerForm.get('nic').valid){
        this.step1 = false;
        this.step2 = true;
        this.step3 = false;
      }

    }
  }

  getverify() {
    this.step1 = false;
    this.step2 = false;
    this.step3 = true;
  }

  validateForm() {
    if (
      trim(this.registerForm.get('fname').value) !== '' && this.registerForm.get('fname').valid &&
      trim(this.registerForm.get('lname').value) !== '' && this.registerForm.get('lname').valid &&
      trim(this.registerForm.get('nic').value) !== '' && this.registerForm.get('nic').valid &&
      trim(this.registerForm.get('email').value) !== '' && this.registerForm.get('email').valid &&
      trim(this.registerForm.get('password').value) !== '' && this.registerForm.get('password').valid
    ) {
      return true;
    } else {
      return false;
    }
  }

}
