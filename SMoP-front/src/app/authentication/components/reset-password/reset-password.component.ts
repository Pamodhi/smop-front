import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  resetForm: FormGroup;
  otpForm: FormGroup;
  newPassForm: FormGroup;
  newPass=false;
  accverify=true;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.formBuilder();
  }

  formBuilder() {
    this.resetForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    });

    this.otpForm = new FormGroup({
      otpCode: new FormControl("", Validators.required)
    });

    this.newPassForm = new FormGroup({
      password: new FormControl("", Validators.required),
      repassword: new FormControl("", Validators.required)
    });

  }

  getOTPCode() {
    if(this.resetForm.controls.email.valid){
      this.authService.sendCode(this.resetForm.get('email').value).subscribe(
        (data) => {
          if (data['msg'] == true) {
            localStorage.setItem('id',data['userId']);
            this.resetForm.controls.email.disable();
            alert("Enter otp code")
          }else{
            alert("user did not exist")
          }
        },
        err=>{
          console.log(err);

        }
      );
    }
  }
  verifyOTP(){
    this.authService.verifyUser(this.otpForm.get('otpCode').value).subscribe(
      data=>{
        if(data['msg']==true){
          this.accverify=false;
          this.newPass =true;
        }else{
          alert('otp code is incorrect')
        }
      }
    );
  }

  resetPassword(){
    if(this.newPassForm.get('password').value == this.newPassForm.get('repassword').value){
    }else{
      this.newPassForm.get('repassword').value=="";

    }
  }

}
