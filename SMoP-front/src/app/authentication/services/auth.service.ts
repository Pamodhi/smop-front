import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Register } from '../models/register.model'
import { EmailValidator } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  config = {
    headers: {
      userId: localStorage.getItem('id')
    }
  }

  userLogin(user: object) {
    return this.http.post(environment.apiUrl + "/LoginManager/login", user);
  }
  createUser(userDetails: object) {
    /* const formData: FormData = new FormData();
     formData.append('image', user.image);
     formData.append('fname', JSON.stringify(user.fname));
     formData.append('lname', JSON.stringify(user.lname));
     formData.append('nic', JSON.stringify(user.nic));
     formData.append('email', JSON.stringify(user.email));
     formData.append('password', JSON.stringify(user.fname));*/
    return this.http.post(environment.apiUrl + "/ProfilesManager/createUser", userDetails);
  }

  checkUserExist(nic: string) {
    return this.http.get(environment.apiUrl + "/ProfilesManager/userExist?nic=" + nic);
  }

  checkEmailExist(email: string) {
    return this.http.get(environment.apiUrl + "/ProfilesManager/emailExist?email=" + email);
  }

  verifyUser(code: string){
    return this.http.get(environment.apiUrl + "/ProfilesManager/verifyCode?code=" + code, this.config);
  }
  sendCode(email:string){
    return this.http.get(environment.apiUrl+"/ProfilesManager/sendCode?email=" +email, this.config);
  }
  verifyCode(code){
    return this.http.get(environment.apiUrl+"/ProfilesManager/verifyCode?code=" +code, this.config);
  }

}
