import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './theme/layout/admin/admin.component';
import { LoginComponent } from './authentication/components/login/login.component';
import { RegisterComponent } from './authentication/components/register/register.component';
import { IncomeComponent } from './dashboard/transaction/components/income/income.component';
import { RedirectGuard } from './guard/redirect.guard';
import {ResetPasswordComponent} from './authentication/components/reset-password/reset-password.component'
  import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'signin',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: AdminComponent,
    loadChildren: () => import('./dashboard/dashboard.module').then(module => module.DashboardModule)
  },
  {
    path: 'signin',
    canActivate: [RedirectGuard],
    component: LoginComponent,
  },
  // Auth
  {
    path: 'signup',
    canActivate: [RedirectGuard],
    component: RegisterComponent
  },
  {
    path: 'reset-password',
    canActivate: [RedirectGuard],
    component: ResetPasswordComponent
  },
  {
    path: '**',
    redirectTo: 'signin',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
