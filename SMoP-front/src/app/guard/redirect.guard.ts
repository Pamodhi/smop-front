import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonService } from '../services/common.service';
import { NavigationItem } from '../theme/layout/admin/navigation/navigation';

@Injectable({
  providedIn: 'root'
})
export class RedirectGuard implements CanActivate {
  UserToken = localStorage.getItem('token');
  UserId = localStorage.getItem('id');


  constructor(
    private permissionService: CommonService,
    private nav: NavigationItem,
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Observable<boolean>(obs => {
      if (this.UserToken && this.UserId) {
        obs.next(false);
        this.router.navigateByUrl('/dashboard/home');
      } else {
        obs.next(true);
        // this.router.navigateByUrl('/signin');
      }
    });
  }
}
