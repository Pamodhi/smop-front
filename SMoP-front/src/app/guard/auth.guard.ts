import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CommonService } from '../services/common.service';
import { NavigationItem } from '../theme/layout/admin/navigation/navigation';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  roles = environment.userPermittion;
  public navigation: any;
  access = false;
  permission_nav: any;
  reqUrl = '';

  constructor(
    private permissionService: CommonService,
    private nav: NavigationItem,
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.navigation = this.nav.get();
    this.reqUrl = state.url;

    return new Observable<boolean>(obs => {
      // obs.next(true);
      // console.log(obs.next());
      if (!localStorage.getItem('token') && !localStorage.getItem('id')) {
        localStorage.clear();
        this.router.navigateByUrl('/signin');
      }
      this.permissionService.getPermission(localStorage.getItem('id')).subscribe(
        data => {
          let role = data['userType'];
          this.roles.map((roleObj) => {
            if (data['userType'].toString() === roleObj.role.toString()) {
              this.permission_nav = roleObj.nav;
            }
          });

          this.access = false;

          this.navigation.map((nav) => {
            if (nav.type === "group") {
              this.navMap(nav.children);
            }
            if (nav.type === 'collapse') {
              this.navMap(nav.children);
            }
          });
          if (this.access) {
            obs.next(true);
          } else {
            this.router.navigateByUrl('/dashboard/home');
            // obs.next(false);
            // this.router.navigate(['/dashboard/home']);
            // obs.next(false);
          }
        },
        err => {
          if (err.status === 401) {
            localStorage.clear();
            this.router.navigateByUrl('/signin');

          }
        }
      );
    })
  }

  navMap(nav) {

    if (nav.type === 'item') {
      if (this.permission_nav.includes(nav.id)) {
        if (nav.url === this.reqUrl) {
          this.access = true;
        }
      }
    } else {
      nav.map((item) => {
        if (item.type === 'group') {
          this.navMap(item.children);
        }
        if (item.type === 'collapse') {
          this.navMap(item.children);
        }
        if (item.type === 'item') {
          this.navMap(item);
        }
      });
    }
  }
}
