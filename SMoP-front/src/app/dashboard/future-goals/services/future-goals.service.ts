import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import{environment} from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FutureGoalsService {

  constructor(private http:HttpClient) {}

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }

   setfutureGoal(goal :object):Observable<Object>{
    return this.http.post(environment.apiUrl+"/FutureGoal/createGoal",goal,this.config);
  }

  getFutureGoal(){
    return this.http.get(environment.apiUrl+"/FutureGoal/getGoals", this.config)
  }
}
