import { TestBed } from '@angular/core/testing';

import { FutureGoalsService } from './future-goals.service';

describe('FutureGoalsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FutureGoalsService = TestBed.get(FutureGoalsService);
    expect(service).toBeTruthy();
  });
});
