import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FutureGoalsComponent } from './components/future-goals/future-goals.component';
import { SharedModule } from '../../theme/shared/shared.module';



@NgModule({
  declarations: [FutureGoalsComponent],
  imports: [
    SharedModule,
    CommonModule
  ]
})
export class FutureGoalsModule { }
