import { Component, OnInit } from '@angular/core';
import {FutureGoalsService} from '../../services/future-goals.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ApexChartService } from 'src/app/theme/shared/components/chart/apex-chart/apex-chart.service';
import { ChartDB } from 'src/app/fack-db/chart-data';
import { trim } from 'jquery';

@Component({
  selector: 'app-future-goals',
  templateUrl: './future-goals.component.html',
  styleUrls: ['./future-goals.component.scss']
})
export class FutureGoalsComponent implements OnInit {

  goalForm : FormGroup;
  public chartDB: any;
  arrayData;
  series=[];
  labels=[];
  grid= false;
  constructor(private futureGoalService:FutureGoalsService, public apexEvent: ApexChartService) {
    this.chartDB = ChartDB;
   }

  formBuild(){
    this.goalForm = new FormGroup({
      goal_name:new FormControl("", Validators.required),
      goal_toDate : new FormControl("", Validators.required),
      goal_fromDate:new FormControl("", Validators.required),
      goal_amount:new FormControl("", Validators.required),
      goal_note:new FormControl("", Validators.required),
    })
  }

  ngOnInit() {
    this.formBuild();
    this.getfutureGoal();
  }

  create(){
    if(this.validateForm()){
      this.futureGoalService.setfutureGoal(this.goalForm.value).subscribe(
        (date)=>{
          this.formBuild();
          this.setDatagrid();
        },
        (err)=>{

        }
      );
    }else{
      this.goalForm.markAllAsTouched();
    }

  }

  validateForm(){
    if (
      trim(this.goalForm.get('goal_name').value) !== '' && this.goalForm.get('goal_name').valid &&
      trim(this.goalForm.get('goal_toDate').value) !== '' && this.goalForm.get('goal_toDate').valid &&
      trim(this.goalForm.get('goal_fromDate').value) !== '' && this.goalForm.get('goal_fromDate').valid &&
      trim(this.goalForm.get('goal_amount').value) !== '' && this.goalForm.get('goal_amount').valid
    ){
      return true;
    }else{
      return false;
    }
  }

  setDatagrid(){
    this.chartDB.radialBar2CAC.series=this.series;
    this.chartDB.radialBar2CAC.labels=this.labels;
    this.chartDB.radialBar2CAC.colors=['#ac167a','#300723']
  }

  getfutureGoal(){
    this.futureGoalService.getFutureGoal().subscribe(
      data=>{
        this.arrayData=data;
        for (let index = 0; index <this.arrayData.length ; index++) {
          this.series.push(this.arrayData[index]['amount']);
          this.labels.push(this.arrayData[index]['name']);
        }
        this.grid=true;
        this.setDatagrid();
      },
      err=>{

      }
    );
  }
}
