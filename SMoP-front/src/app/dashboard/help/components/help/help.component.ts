import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  emailForm: FormGroup;
  constructor(private router: Router) {

  }
  formBuilder() {
    this.emailForm = new FormGroup({
      email: new FormControl("", Validators.required),
      subject: new FormControl("", Validators.required),
      message: new FormControl("", Validators.required),
    });
  }
  ngOnInit() {
  }

  create(){

  }
}
