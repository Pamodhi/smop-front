import { Injectable } from '@angular/core';

import{HttpClient} from '@angular/common/http';
import{environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private http:HttpClient) { }
  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }

  getBudgetGridData(toDate:string, fromDate:string) {
    return this.http.get(environment.apiUrl+"/ReportData/getBudget?toDate="+toDate+"&fromDate="+fromDate,this.config);
  }

  getAccountGridData(toDate:string, fromDate:string, account: number) {
    return this.http.get(environment.apiUrl+"/ReportData/getAccountTrans?toDate="+toDate+"&fromDate="+fromDate+"&account="+account,this.config);
  }

  getIncomeGridData(toDate:string, fromDate:string) {
    return this.http.get(environment.apiUrl+"/ReportData/getIncome?toDate="+toDate+"&fromDate="+fromDate,this.config);
  }

  getExpenseGridData(toDate:string, fromDate:string) {
    return this.http.get(environment.apiUrl+"/ReportData/getExpenses?toDate="+toDate+"&fromDate="+fromDate,this.config);
  }

  getAccount(){
    return this.http.get(environment.apiUrl+"/AccountManager/getAccount", this.config)
  }
}
