import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './components/reports/reports.component';
import { SharedModule } from '../../theme/shared/shared.module';



@NgModule({
  declarations: [ReportsComponent],
  imports: [
    CommonModule,
    SharedModule,
  ]
})
export class ReportsModule { }
