import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApexChartService } from 'src/app/theme/shared/components/chart/apex-chart/apex-chart.service';
import { ReportsService } from '../../services/reports.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  grid1=false;
  grid2=false;
  grid3=false;
  grid4=false;
  grid1_daterange=false;
  grid2_daterange=false;
  grid3_daterange=false;
  grid4_daterange=false;
  gridForm: FormGroup;
  gridForm2: FormGroup;
  gridForm3: FormGroup;
  gridForm4: FormGroup;
  gridData;
  gridData2;
  gridData3;
  gridData4;
  accountData;

  constructor(public apexEvent: ApexChartService, private router: Router, private reportService: ReportsService) {

   }

  ngOnInit() {
    this.formBuilder();
    this.getAccount();
  }

  formBuilder(){
    this.gridForm = new FormGroup({
      to_date: new FormControl("", Validators.required),
      from_date: new FormControl("", Validators.required)
    });

    this.gridForm2 = new FormGroup({
      to_date: new FormControl("", Validators.required),
      from_date: new FormControl("", Validators.required),
      account: new FormControl("", Validators.required)
    });

    this.gridForm3 = new FormGroup({
      to_date: new FormControl("", Validators.required),
      from_date: new FormControl("", Validators.required)
    });

    this.gridForm4 = new FormGroup({
      to_date: new FormControl("", Validators.required),
      from_date: new FormControl("", Validators.required)
    });
  }

  grid1load(){
    this.grid1=true;
    this.grid1_daterange=true;
  }
  grid2load(){
    this.grid2=true;
    this.grid2_daterange=true;
  }
  grid3load(){
    this.grid3=true;
    this.grid3_daterange=true;
  }
  grid4load(){
    this.grid4=true;
    this.grid4_daterange=true;
  }

  getGridiData(){
    this.reportService.getBudgetGridData(this.gridForm.get('to_date').value, this.gridForm.get('from_date').value).subscribe(
      data=>{
        this.gridData=data;
      },
      err=>{
        console.log(err);

      }
    );
  }
  getGridiData2(){
    this.reportService.getAccountGridData(this.gridForm2.get('to_date').value, this.gridForm2.get('from_date').value, this.gridForm2.get('account').value).subscribe(
      data=>{
        console.log(data);
        this.gridData2=data;
      },
      err=>{
        console.log(err);
      }
    );
  }
  getGridiData3(){
    this.reportService.getExpenseGridData(this.gridForm3.get('to_date').value, this.gridForm3.get('from_date').value).subscribe(
      data=>{
        this.gridData3=data;
      },
      err=>{
        console.log(err);
      }
    );
  }
  getGridiData4(){
    this.reportService.getIncomeGridData(this.gridForm4.get('to_date').value, this.gridForm4.get('from_date').value).subscribe(
      data=>{
        this.gridData4=data;
      },
      err=>{
        console.log(err);
      }
    );
  }

  getAccount() {
    this.reportService.getAccount().subscribe(
      data => {
        console.log(data);

        this.accountData = data;
      },
      err => {
        console.log(err);

      }
    );
  }
}
