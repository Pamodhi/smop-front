import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { identity, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpClient) { }

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }

  //Expence
  getcatDetails() {
    return this.http.get(environment.apiUrl + "/ExpenseManager/getCategoryDetails", this.config);
  }
  createExpense(expense: object) {
    return this.http.post(environment.apiUrl + "/ExpenseManager/createExpense", expense, this.config);
  }
  getExpenseDetails() {
    return this.http.get(environment.apiUrl + "/ExpenseManager/getAllExpenseDetails", this.config);
  }
  currentMonthExp(){
    return this.http.get(environment.apiUrl+"/DashBoadManager/getCurrentMonthExp", this.config)
  }
  deleteExpense(id: number) {
    return this.http.delete(environment.apiUrl + "/ExpenseManager/deleteExpense/" + id, this.config)
  }
  getExpenseDetailsByid(id: number) {
    return this.http.get(environment.apiUrl + "/ExpenseManager/getExpenseById?id=" + id, this.config);
  }
  compareWithLastMonth(catId: number, amount){
    return this.http.get(environment.apiUrl+"/ExpenseManager/compireWithLastMonth?catId="+catId+"&amount="+amount, this.config)
  }

  //income
  createIncome(income: object) {
    return this.http.post(environment.apiUrl + "/IncomeManager/createIncome", income, this.config);
  }
  getIncomeDetails() {
    return this.http.get(environment.apiUrl + "/IncomeManager/getAllIncomeDetails", this.config);
  }

  deleteIncome(id: number) {
    return this.http.delete(environment.apiUrl + "/IncomeManager/deleteIncome/" + id, this.config)
  }

  updateIncome(id: number, income: object) {
    return this.http.put(environment.apiUrl + "/IncomeManager/updateIncome/" + id, income,this.config);
  }

  getIncomeDetailsByid(id: number) {
    return this.http.get(environment.apiUrl + "/IncomeManager/getIncomeById?id=" + id, this.config);
  }

  currentMonthInc(){
    return this.http.get(environment.apiUrl+"/DashBoadManager/getCurrentMonthInc", this.config)
  }

  //bill
  createBill(expense: object) {
    return this.http.post(environment.apiUrl +"/BillPayment/createBill", expense, this.config);
  }

  getBill(){
    return this.http.get(environment.apiUrl+"/BillPayment/getBillDetails", this.config)
  }

  deleteBill(id: number){
    return this.http.delete(environment.apiUrl + "/BillPayment/deleteBill/" + id, this.config)
  }

  payBill(id: number){
    return this.http.put(environment.apiUrl + "/IncomeManager/updateIncome/" + id,this.config);
  }

  //common
  getAccount(){
    return this.http.get(environment.apiUrl+"/AccountManager/getAccount", this.config)
  }
  getPayee(){
    return this.http.get(environment.apiUrl+"/PayeeManager/getPayee", this.config)
  }


}
