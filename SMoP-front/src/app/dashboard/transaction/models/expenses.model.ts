export class Expenses {
  expen_category: string;
  expen_date: string;
  expen_amount: string;
  expen_account: string;
  expen_payee: string;
  expen_note: string;
}
