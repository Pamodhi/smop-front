import { Component, OnInit } from '@angular/core';

// fake data
import { ApexChartService } from 'src/app/theme/shared/components/chart/apex-chart/apex-chart.service';
import { ChartDB } from 'src/app/fack-db/chart-data';
import { Router } from '@angular/router';
import { TransactionService } from '../../services/transaction.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { trim } from 'jquery';
// fake data

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit {

  public chartDB: any;
  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;
  public deviceProgressBar: any;

  expensesForm: FormGroup;

  catData;
  accountData;
  payeeData;
  billData;
  constructor(public apexEvent: ApexChartService, private router: Router,
    private transactionServices:TransactionService) {
    if (localStorage.getItem('token') === '' || localStorage.getItem('token') === null) {
      this.router.navigateByUrl('/signin');
     }
    this.chartDB = ChartDB;
    this.dailyVisitorStatus = '1y';

    this.deviceProgressBar = [
      {
        type: 'success',
        value: 66
      }, {
        type: 'primary',
        value: 26
      }, {
        type: 'danger',
        value: 8
      }
    ];
  }

  formBuilder() {
    this.expensesForm = new FormGroup({
      expen_category: new FormControl("", Validators.required),
      expen_date: new FormControl("", Validators.required),
      expen_amount: new FormControl("", Validators.required),
      expen_note: new FormControl("", Validators.required),
      expen_account: new FormControl("", Validators.required),
      expen_payee: new FormControl("", Validators.required),
    });
  }
  ngOnInit() {
    this.formBuilder();
    this.getBillDetails();
    this.getCatagoryDetails();
    this.getAccount();
    this.getPayee();
  }

  onSubmit() {
    if(this.validateForm()){
      this.transactionServices.createBill(this.expensesForm.value).subscribe(
        (data) => {
          console.log(data);
          alert("successfully submited !")
          this.getBillDetails();
          this.expensesForm.reset();
        },
        (err) => {}
        );
    }else{
      this.expensesForm.controls.expen_category.markAllAsTouched();
      this.expensesForm.controls.expen_amount.markAllAsTouched();
      this.expensesForm.controls.expen_date.markAllAsTouched();
    }
  }

  getCatagoryDetails(){
    this.transactionServices.getcatDetails().subscribe(
      (data) =>{
        this.catData =data;
        console.log(data);
      },
      (err) => {
        console.log(err);

      }
    );
  }

  getAccount(){
    this.transactionServices.getAccount().subscribe(
      data=>{
        this.accountData =data;
      },
      err=>{
        console.log(err);

      }
    );
  }

  getPayee(){
    this.transactionServices.getPayee().subscribe(
      data=>{
        console.log(data);

        this.payeeData =data;
      },
      err=>{
        console.log(err);

      }
    );
  }

  getBillDetails(){
    this.transactionServices.getBill().subscribe(
      data=>{
        this.billData =data;
      },
      err=>{
        console.log(err);

      }
    );
  }
  deleteData(id){
    var status = confirm("Do you want to delete");
    if (status) {
      this.transactionServices.deleteBill(id).subscribe(
        (data) => {
          alert("Successfully Deleted !");
          this.ngOnInit();
        },
        (err) => {
          alert("error");
          console.log(err);
        }
      );
    }
  }

  payBill(id){
    this.transactionServices.payBill(id).subscribe(
      data=>{
        alert("Successfully paid");
        this.ngOnInit();
      },
      err=>{
        console.log(err);

      }
    );
  }

  validateForm(){
    if (
      trim(this.expensesForm.get('expen_category').value) !== '' && this.expensesForm.get('expen_category').valid &&
      trim(this.expensesForm.get('expen_date').value) !== '' && this.expensesForm.get('expen_date').valid &&
      trim(this.expensesForm.get('expen_amount').value) !== '' && this.expensesForm.get('expen_amount').valid
    ){
      return true;
    }else{
      return false;
    }
  }
}
