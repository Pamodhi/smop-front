import { Component, OnInit } from "@angular/core";
import { TransactionService } from "../../services/transaction.service"

// fake data
import { ApexChartService } from "src/app/theme/shared/components/chart/apex-chart/apex-chart.service";
import { ChartDB } from "src/app/fack-db/chart-data";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Transaction } from '../../models/transaction.model';
import { Router } from "@angular/router";
import { data, trim } from "jquery";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
// fake data

@Component({
  selector: "app-expenses",
  templateUrl: "./expenses.component.html",
  styleUrls: ["./expenses.component.scss"],
})
export class ExpensesComponent implements OnInit {
  public chartDB: any;
  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;
  public deviceProgressBar: any;

  expensesForm: FormGroup;
  expenseDataByid;
  addExpenseForm: Boolean = true;
  expenseChart: Boolean = true;
  expensegrid: Boolean = false;
  saveBtn: Boolean = true;
  updateBtn: Boolean = false;
  totalExp = '0';

  test;

  readOnly = false;
  expenseData;
  catData;
  accountData;
  payeeData;
  submitted = false;
  amount;
  date;
  date1;

  constructor(public apexEvent: ApexChartService, private transactionServices: TransactionService,
    private router: Router) {

    this.chartDB = ChartDB;
    this.dailyVisitorStatus = "1y";

    this.deviceProgressBar = [
      {
        type: "success",
        value: 66,
      },
      {
        type: "primary",
        value: 26,
      },
      {
        type: "danger",
        value: 8,
      },
    ];
  }

  formBuilder() {
    this.expensesForm = new FormGroup({
      expen_category: new FormControl("", Validators.required),
      expen_date: new FormControl("", Validators.required),
      expen_amount: new FormControl("", Validators.required),
      expen_note: new FormControl("", Validators.required),
      expen_account: new FormControl("", Validators.required),
      expen_payee: new FormControl("", Validators.required),
      expen_id: new FormControl("", Validators.required),
    });
  }
  ngOnInit() {
    this.formBuilder();
    this.getCatagoryDetails();
    this.getExpenseDatails();
    this.getCurrentMonthExp();
    this.getAccount();
    this.getPayee();
    this.test = new Date();
    console.log(this.test);
    const day = { day: this.test.getUTCDate(), month: this.test.getUTCMonth(), year: this.test.getUTCFullYear() }


  }

  addgrid() {
    this.addExpenseForm = false;
    this.expensegrid = true;
  }

  viewAdd() {
    this.addExpenseForm = true;
    this.expenseChart = true;
    this.expensegrid = false;
    this.updateBtn = false;
    this.saveBtn = true;
    this.expensesForm.reset();
  }

  onSubmit() {
    if (this.validateForm()) {
      this.transactionServices.createExpense(this.expensesForm.value).subscribe(
        (data) => {
          if (data['msg'] == "Success") {
            this.submitted = true;
            alert('Succesfully Created');
            this.expensesForm.reset();
          } else {
            alert('Error');
          }
        },
        (err) => { alert(err); }

      );
    } else {
      this.expensesForm.markAllAsTouched();
    }
  }
  getCatagoryDetails() {
    this.transactionServices.getcatDetails().subscribe(
      (data) => {
        this.catData = data;
        console.log(data);
      },
      (err) => { }
    );
  }

  deleteData(id: number) {
    var status = confirm("Do you want to delete");
    if (status) {
      this.transactionServices.deleteIncome(id).subscribe(
        (data) => {
          alert("Successfully Deleted !");
          this.ngOnInit();
        },
        (err) => {
          alert("error");
          console.log(err);
        }
      );
    }

  }

  getExpenseDatails() {
    this.transactionServices.getExpenseDetails().subscribe(
      (data) => {
        this.expenseData = data;
        console.log(data);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getCurrentMonthExp() {
    this.transactionServices.currentMonthExp().subscribe(
      (data) => {
        console.log(data['SumAmount']);
        this.totalExp = data['SumAmount'];
      },
      (err) => {
        this.totalExp = '0';
        console.log(err);
      }
    );
  }

  getExpenseData(id: number) {
    var status = confirm("Do you want to Update");
    if (status) {
      this.addExpenseForm = true;
      this.expenseChart = false;
      this.updateBtn = true;
      this.saveBtn = false;
      this.transactionServices.getExpenseDetailsByid(id).subscribe(
        (data) => {
          console.log(data);

          this.expensesForm.controls.expen_id.setValue(data['id']);
          this.expensesForm.controls.expen_amount.setValue(data['amount']);
          this.expensesForm.controls.expen_date.setValue(data['date']);
          this.expensesForm.controls.expen_note.setValue(data['des']);
          this.expensesForm.controls.expen_payee.setValue(data['payee']);
          this.expensesForm.controls.expen_account.setValue(data['account']);
          this.expensesForm.controls.expen_category.setValue(data['category']);
        },
        (err) => {
          alert("error");
        }
      );
    }
  }

  onUpdate() {
    if (this.validateForm()) {
      this.transactionServices.updateIncome(this.expensesForm.get('expen_id').value, this.expensesForm.value).subscribe(
        (data) => {
          this.formBuilder();
          this.getExpenseDatails();
          this.getCurrentMonthExp();
          this.getAccount();
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      this.expensesForm.markAllAsTouched();
    }
  }

  clear() {
    this.expensesForm.reset();
  }

  getAccount() {
    this.transactionServices.getAccount().subscribe(
      data => {
        this.accountData = data;
      },
      err => {
        console.log(err);

      }
    );
  }

  getPayee() {
    this.transactionServices.getPayee().subscribe(
      data => {
        console.log(data);

        this.payeeData = data;
      },
      err => {
        console.log(err);

      }
    );
  }


  /*checkAmount(myPersistenceModal){
    myPersistenceModal.show();*/

  compareWithLastMonth(myPersistenceModal) {
    this.transactionServices.compareWithLastMonth(this.expensesForm.get('expen_category').value, this.expensesForm.get('expen_amount').value).subscribe(
      data => {
        if (data['response']) {
          myPersistenceModal.show();
          this.amount = data['amount'];
        } else {
          this.onSubmit();
        }
      },
      err => {
        console.log(err);

      }
    );
  }

  validateForm() {
    if (
      trim(this.expensesForm.get('expen_category').value) !== '' && this.expensesForm.get('expen_category').valid &&
      trim(this.expensesForm.get('expen_date').value) !== '' && this.expensesForm.get('expen_date').valid &&
      trim(this.expensesForm.get('expen_amount').value) !== '' && this.expensesForm.get('expen_amount').valid
    ) {
      return true;
    } else {
      return false;
    }
  }
}
