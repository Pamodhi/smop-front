import { Component, OnInit } from '@angular/core'
import { TransactionService } from '../../services/transaction.service'
import { Income } from '../../models/income.model'

// fake data
import { ApexChartService } from 'src/app/theme/shared/components/chart/apex-chart/apex-chart.service';
import { ChartDB } from 'src/app/fack-db/chart-data';
// fake data end
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { trim } from 'jquery';


@Component({
  selector: 'app-income',
  templateUrl: './income.component.html',
  styleUrls: ['./income.component.scss']
})
export class IncomeComponent implements OnInit {

  public chartDB: any;
  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;
  public deviceProgressBar: any;

  incomeForm: FormGroup;
  income: Income = new Income();
  submitted = false;
  incomeData;
  incomeDataByid;
  addIncomeForm: Boolean = true;
  incomeChart: Boolean = true;
  incomegrid: Boolean = false;
  saveBtn: Boolean = true;
  updateBtn: Boolean = false;
  totalInc = '0';
  accountData;

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('userId')
    }
  }

  constructor(public apexEvent: ApexChartService, private route: ActivatedRoute, private router: Router,
    private transactionServices: TransactionService) {
    this.chartDB = ChartDB;
    this.dailyVisitorStatus = '1y';

    this.deviceProgressBar = [
      {
        type: 'success',
        value: 66
      }, {
        type: 'primary',
        value: 26
      }, {
        type: 'danger',
        value: 8
      }
    ];
  }

  formBuilder() {
    this.incomeForm = new FormGroup({
      income_des: new FormControl("", Validators.required),
      income_date: new FormControl("", Validators.required),
      income_amount: new FormControl("", Validators.required),
      income_account: new FormControl("", Validators.required),
      income_id: new FormControl("", Validators.required),
    })
  }

  ngOnInit() {
    this.formBuilder();
    this.getIncomeDetails();
    this.getCurrentMonthInc();
    this.getAccount();
  }

  getCurrentMonthInc() {
    this.transactionServices.currentMonthInc().subscribe(
      (data) => {
        this.totalInc = data['SumAmount'];
      },
      (err) => {
        this.totalInc = '0';
        console.log(err);
      }
    );
  }


  onSubmit() {
    if (this.validateForm()) {
      this.transactionServices.createIncome(this.incomeForm.value).subscribe(
        (data) => {
          if(data['msg']=="Success"){
            this.submitted=true;
            this.formBuilder();
            this.getIncomeDetails();
            this.getCurrentMonthInc();
            this.getAccount();
            alert('Succesfully Created');
            this.incomeForm.reset();
          }else{
            alert('Error');
          }
        },
        (err) => {
          alert("Error");
        }
      );
    } else {
      this.incomeForm.controls.income_amount.markAllAsTouched();
      this.incomeForm.controls.income_date.markAllAsTouched();
      this.incomeForm.controls.income_des.markAllAsTouched();
      //this.incomeForm.markAllAsTouched();
    }

  }


  getIncomeDetails() {

    this.transactionServices.getIncomeDetails().subscribe(
      (data) => {
        this.incomeData = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  deleteData(id: number) {
    var status = confirm("Do you want to delete");
    if (status) {
      this.transactionServices.deleteIncome(id).subscribe(
        (data) => {
          alert("Successfully Deleted !");
          this.ngOnInit();
        },
        (err) => {
          alert("error");
          console.log(err);
        }
      );
    }
  }

  gotoList() {
    this.router.navigate(['/dashboard']);
  }

  getIncomeData(id: number) {
    var status = confirm("Do you want to Update");
    if (status) {
      this.addIncomeForm = true;
      this.incomeChart = false;
      this.updateBtn = true;
      this.saveBtn = false;
      this.transactionServices.getIncomeDetailsByid(id).subscribe(
        (data) => {
          this.incomeForm.controls.income_amount.setValue(data['amount']);
          this.incomeForm.controls.income_date.setValue(data['date']);
          this.incomeForm.controls.income_des.setValue(data['des']);
          this.incomeForm.controls.income_id.setValue(data['id']);
          this.incomeForm.controls.income_account.setValue(data['account']);
          this.incomeForm.controls.income_account.setValue(data['acc_id']);
          this.incomeForm.controls.income_account.disable();
        },
        (err) => {
          alert("error");
        }
      );
    }
  }

  onUpdate() {
    if (this.validateForm()) {
    this.transactionServices.updateIncome(this.incomeForm.get('income_id').value, this.incomeForm.value).subscribe(
      (data) => {
        this.formBuilder();
        this.getIncomeDetails();
        this.getCurrentMonthInc();
        this.getAccount();
      },
      (err) => {
        console.log(err);
      }
    );
    }else{
      this.incomeForm.controls.income_amount.markAllAsTouched();
    }
  }

  addgrid() {
    this.addIncomeForm = false;
    this.incomegrid = true;
  }
  viewAdd() {
    this.addIncomeForm = true;
    this.incomeChart = true;
    this.incomegrid = false;
    this.updateBtn=false;
    this.saveBtn=true;
    this.incomeForm.reset();
    this.incomeForm.controls.income_account.enable();
  }

  getAccount() {
    this.transactionServices.getAccount().subscribe(
      data => {
        this.accountData = data;
      },
      err => {
        console.log(err);

      }
    );
  }

  validateForm(){
    if (
      trim(this.incomeForm.get('income_des').value) !== '' && this.incomeForm.get('income_des').valid &&
      trim(this.incomeForm.get('income_date').value) !== '' && this.incomeForm.get('income_date').valid &&
      trim(this.incomeForm.get('income_amount').value) !== '' && this.incomeForm.get('income_amount').valid
    ){
      return true;
    }else{
      return false;
    }
  }

}
