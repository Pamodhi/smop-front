import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../theme/shared/shared.module';
import { RouterModule } from '@angular/router';
import { NgbPopoverModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

import { IncomeComponent } from './components/income/income.component';
import { ExpensesComponent } from './components/expenses/expenses.component';
import { BillComponent } from './components/bill/bill.component';



@NgModule({
  declarations: [
    IncomeComponent,
    ExpensesComponent,
    BillComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    NgbPopoverModule,
    NgbProgressbarModule,
  ]
})
export class TransactionModule { }
