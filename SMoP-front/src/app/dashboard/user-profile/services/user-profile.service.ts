import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  constructor( private http: HttpClient) {}

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }

  getUserDetails(){
    return this.http.get(environment.apiUrl+"/ProfilesManager/getUserDetails",this.config);
  }

  updateUser(){

  }
}
