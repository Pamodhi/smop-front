import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserProfileService } from '../../services/user-profile.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  userForm=false;
  userDetails=true;
  profession;
  userName;
  email;
  prof;
  nic;
  curncy;
  constructor(private router: Router,private userProfileService: UserProfileService) { }

  ngOnInit() {
    this.getUserDetails();
  }

  editData(){
    this.userForm=true;
    this.userDetails=false;
  }

  getUserDetails(){
    this.userProfileService.getUserDetails().subscribe(
      data=>{
        console.log(data);

        this.userName=data['fname'];
        this.email =data['email'];
        this.nic =data['nic'];
        this.curncy =data['curncy'];
        this.prof =data['prof'];
        this.profession =data['prof'];
      },
      err=>{

      }
    );
  }

}
