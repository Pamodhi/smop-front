import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


// import { FlexLayoutModule } from "@angular/flex-layout";
//import { NgxEventCalendarModule } from "ngx-event-calendar";

import { CalendarComponent } from './components/calendar/calendar.component';

@NgModule({
  declarations: [
    CalendarComponent
  ],
  imports: [
    CommonModule,
    // BrowserModule,
    // BrowserAnimationsModule
  ],
  exports: [
    CalendarComponent
  ]
})
export class CalendarModule { }
