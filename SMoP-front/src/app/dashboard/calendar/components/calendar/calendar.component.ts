import { Component, OnInit } from '@angular/core';
import { EventData } from 'ngx-event-calendar/lib/interface/event-data';
export const testData: EventData[] = [
  {
    id: 20,
    title: "Match",
    desc: "BL Match",
    startDate: new Date("2021-01-22T21:00:00"),
    endDate: new Date("2021-01-22T23:00:00"),
    createdBy: "Mark",
    createdAt: new Date("2021-01-21T10:00:00"),
    type: 2,
    color: "red"
  }
];
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  constructor() { }

  ngOnInit() { }
  // dataArray: EventData[] = testData;

  selectDay(event) {
    console.log(event);
  }
  addEvent(){}
}
