import { Component, OnInit } from '@angular/core';


// fake data
import { ApexChartService } from 'src/app/theme/shared/components/chart/apex-chart/apex-chart.service';
import { ChartDB } from 'src/app/fack-db/chart-data';
import { Router } from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';
// fake data

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public chartDB: any;
  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;
  public deviceProgressBar: any;

  sumExp='0';
  sumInc='0';
  sumBill='0';
  accounts='0'
  dashboadType=true;
  type;

  constructor(public apexEvent: ApexChartService, private router: Router, private dashboardService: DashboardService) {
    this.chartDB = ChartDB;
    this.dailyVisitorStatus = '1y';

    this.deviceProgressBar = [
      {
        type: 'success',
        value: 66
      }, {
        type: 'primary',
        value: 26
      }, {
        type: 'danger',
        value: 8
      }
    ];
  }

  ngOnInit() {
    this.checkAccesType();
    this.getCurrentMonthExp();
    this.getCurrentMonthInc();
    this.getCurrentMonthBill();
    this.getAccountBalanc();
    //this.checkTransaction();
  }

  checkAccesType(){
    this.type = localStorage.getItem('accessType');
    if(this.type == 1){
      this.dashboadType =true;
    }else{
      this.dashboadType =false;
    }
  }

  getCurrentMonthExp(){
    this.dashboardService.currentMonthExp().subscribe(
      (data)=>{
        console.log(data['SumAmount']);
        this.sumExp=data['SumAmount'];

      },
      (err)=>{
        this.sumExp='0';
        console.log(err);
      }
    );
  }

  getCurrentMonthInc(){
    this.dashboardService.currentMonthInc().subscribe(
      (data)=>{
        console.log(data['SumAmount']);
        this.sumInc=data['SumAmount'];
      },
      (err)=>{
        this.sumInc='0';
        console.log(err);
      }
    );
  }

  getCurrentMonthBill(){
    this.dashboardService.currentMonthBill().subscribe(
      (data)=>{
        console.log(data['SumAmount']);
        this.sumBill=data['SumAmount'];
      },
      (err)=>{
        this.sumBill='0';
        console.log(err);
      }
    );
  }
  getAccountBalanc(){
    this.dashboardService.accountBalance().subscribe(
      (data)=>{
        console.log(data['accountBalance']);
        this.accounts=data['accountBalance'];
      },
      (err)=>{
        this.sumInc='0';
        console.log(err);
      }
    );
  }

  checkTransaction(){
    this.dashboardService.checkTransaction().subscribe(
      (data)=>{
      },
      (err)=>{
        console.log(err);
      }
    );
  }

}
