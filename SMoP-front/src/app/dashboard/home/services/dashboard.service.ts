import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }

  currentMonthExp(){
    return this.http.get(environment.apiUrl+"/DashBoadManager/getCurrentMonthExp", this.config)
  }
  currentMonthInc(){
    return this.http.get(environment.apiUrl+"/DashBoadManager/getCurrentMonthInc", this.config)
  }
  currentMonthBill(){
    return this.http.get(environment.apiUrl+"/DashBoadManager/getCurrentMonthBill", this.config)
  }
  accountBalance(){
    return this.http.get(environment.apiUrl+"/DashBoadManager/getAccountBalance", this.config)
  }

  checkTransaction(){
    return this.http.get(environment.apiUrl+"/DashBoadManager/checkTransactions", this.config)
  }
}
