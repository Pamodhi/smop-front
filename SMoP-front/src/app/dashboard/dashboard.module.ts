import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { HomeModule } from "./home/home.module";
import { TransactionModule } from "./transaction/transaction.module";
import { AccountsModule } from "./accounts/accounts.module";
import { PayeesModule } from "./payees/payees.module";
import { FutureGoalsModule } from "./future-goals/future-goals.module";
import { HelpModule } from "./help/help.module";
import { ReportsModule } from "./reports/reports.module";
import { FamilySyncModule } from './family-sync/family-sync.module';
import { AdminDashboardModule } from './admin-dashboad/admin-dashboad.module';

import { CalendarModule } from './calendar/calendar.module';
import {UserProfileModule} from './user-profile/user-profile.module'

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HomeModule,
    TransactionModule,
    AccountsModule,
    PayeesModule,
    FutureGoalsModule,
    HelpModule,
    ReportsModule,
    FamilySyncModule,
    AdminDashboardModule,
    CalendarModule,
    UserProfileModule
  ]
})
export class DashboardModule { }
