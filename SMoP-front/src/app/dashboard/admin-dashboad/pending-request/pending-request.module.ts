import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../../theme/shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { NgbPopoverModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

import { PendingRequestComponent } from './components/pending-request/pending-request.component';
import { PendingRequestService } from './services/pending-request.service';



@NgModule({
  declarations: [
    PendingRequestComponent
  ],
  imports: [
    CommonModule, SharedModule, ReactiveFormsModule,NgbPopoverModule,NgbProgressbarModule
  ],
  providers:[
    PendingRequestService
  ]
})
export class PendingRequestModule { }
