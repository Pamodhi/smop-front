import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class PendingRequestService {
  constructor(private http: HttpClient) {}

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }

  getUser() {
    return this.http.get(environment.apiUrl+"/AdminManager/getUsers",this.config);
  }

  confirmUser(id: number){
    return this.http.get(environment.apiUrl+"/AdminManager/getApprove?id=" +id,this.config);
  }

  rejectUser(id: number){
    return this.http.get(environment.apiUrl+"/AdminManager/getRegject?id=" +id,this.config);
  }
}
