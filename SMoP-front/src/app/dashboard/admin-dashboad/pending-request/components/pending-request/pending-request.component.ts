import { Component, OnInit } from "@angular/core";
import { PendingRequestService } from "../../services/pending-request.service";

@Component({
  selector: "app-pending-request",
  templateUrl: "./pending-request.component.html",
  styleUrls: ["./pending-request.component.scss"],
})
export class PendingRequestComponent implements OnInit {
  userData ;

  constructor(private pendingRqService: PendingRequestService) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.pendingRqService.getUser().subscribe(
      (data) => {
        this.userData =data;
        console.log(data);
      },
      (err) => {}
    );
  }

  approve(id){
    this.pendingRqService.confirmUser(id).subscribe(
      data=>{
        alert("Successfully submitted!")
        this.getUsers();
      },
      err=>{
        console.log(err);

      }
    );

  }

  reject(id){
    this.pendingRqService.rejectUser(id).subscribe(
      data=>{
        alert("Successfully submitted!")
        this.getUsers();
      },
      err=>{
        console.log(err);

      }
    );
  }
}
