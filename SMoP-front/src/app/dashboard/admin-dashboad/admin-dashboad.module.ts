import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PendingRequestModule } from './pending-request/pending-request.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PendingRequestModule,
  ]
})
export class AdminDashboardModule { }
