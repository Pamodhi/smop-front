import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// components
import { DashboardComponent } from './home/components/dashboard/dashboard.component';
import { IncomeComponent } from './transaction/components/income/income.component';
import { ExpensesComponent } from './transaction/components/expenses/expenses.component';
import { BillComponent } from './transaction/components/bill/bill.component';
import { AccountComponent } from './accounts/components/account/account.component';
import { PayeesComponent } from './payees/components/payees/payees.component';
import { FutureGoalsComponent } from './future-goals/components/future-goals/future-goals.component';
import { HelpComponent } from './help/components/help/help.component';
import { ReportsComponent } from './reports/components/reports/reports.component';
import { FamilySyncComponent } from './family-sync/components/family-sync/family-sync.component';
import { PendingRequestComponent } from './admin-dashboad/pending-request/components/pending-request/pending-request.component';
import { AuthGuard } from '../guard/auth.guard';
import { CalendarComponent } from './calendar/components/calendar/calendar.component';
import { UserProfileComponent} from './user-profile/components/user-profile/user-profile.component'


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: DashboardComponent
  },
  {
    path: 'transaction/income',
    canActivate: [AuthGuard],
    component: IncomeComponent
  },
  {
    path: 'transaction/expenses',
    canActivate: [AuthGuard],
    component: ExpensesComponent
  },
  {
    path: 'transaction/bill',
    canActivate: [AuthGuard],
    component: BillComponent
  },
  {
    path: 'accounts',
    canActivate: [AuthGuard],
    component: AccountComponent
  },
  {
    path: 'payees',
    canActivate: [AuthGuard],
    component: PayeesComponent
  },
  {
    path: 'future-goals',
    canActivate: [AuthGuard],
    component: FutureGoalsComponent
  },
  {
    path: 'help',
    canActivate: [AuthGuard],
    component: HelpComponent
  },
  {
    path: 'reports',
    canActivate: [AuthGuard],
    component: ReportsComponent
  },
  {
    path: 'family-sync',
    canActivate: [AuthGuard],
    component: FamilySyncComponent
  },
  {
    path: 'pending-request',
    canActivate: [AuthGuard],
    component: PendingRequestComponent

  },
  {
    path: 'calendar',
    // canActivate: [AuthGuard],
    component: CalendarComponent

  },
  {
    path: 'user-profile',
    // canActivate: [AuthGuard],
    component: UserProfileComponent
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
