import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AccountsService} from "../../services/accounts.service";
import { trim } from "jquery";

@Component({
  selector: "app-account",
  templateUrl: "./account.component.html",
  styleUrls: ["./account.component.scss"],
})
export class AccountComponent implements OnInit {

  accountForm: FormGroup;

  constructor(private accountService:AccountsService) {
  }

  formBuilder() {
    this.accountForm = new FormGroup({
      account_name: new FormControl("", Validators.required),
      account_no: new FormControl("", Validators.required),
      account_balance: new FormControl("", Validators.required),
      currency: new FormControl("", Validators.required),
    });
  }

  ngOnInit() {
    this.formBuilder();
  }

  onSubmit(){
    if(this.validateForm()){
      this.accountService.createAccount(this.accountForm.value).subscribe(
        data=>{
          alert("Successfully created !");
          this.accountForm.reset();
          console.log(data);
        },
        err=>{
          console.log(err);
        }
      );
    }else{
      this.accountForm.markAllAsTouched();
    }
  }

  validateForm(){
    if (
      trim(this.accountForm.get('account_name').value) !== '' && this.accountForm.get('account_name').valid &&
      trim(this.accountForm.get('account_no').value) !== '' && this.accountForm.get('account_no').valid &&
      trim(this.accountForm.get('account_balance').value) !== '' && this.accountForm.get('account_balance').valid &&
      trim(this.accountForm.get('currency').value) !== '' && this.accountForm.get('currency').valid
    ){
      return true;
    }else{
      return false;
    }
  }
}
