import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import{environment} from 'src/environments/environment';
import { identity, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(private http:HttpClient) {}

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }

  createAccount(account :object):Observable<Object> {
    return this.http.post(environment.apiUrl+"/AccountManager/createAccount",account,this.config);
  }

}
