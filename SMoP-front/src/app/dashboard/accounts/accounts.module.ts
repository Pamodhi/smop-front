import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../theme/shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { NgbPopoverModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

import { AccountComponent } from './components/account/account.component';



@NgModule({
  declarations: [AccountComponent],
  imports: [
    CommonModule,SharedModule, ReactiveFormsModule, NgbPopoverModule,NgbProgressbarModule
  ]
})
export class AccountsModule { }
