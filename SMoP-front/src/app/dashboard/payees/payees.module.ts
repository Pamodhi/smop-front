import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../theme/shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { NgbPopoverModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';

import { PayeesComponent } from "./components/payees/payees.component";
import { PayeesService } from "./services/payees.service";

@NgModule({
  declarations: [PayeesComponent],
  imports: [CommonModule, SharedModule, ReactiveFormsModule,NgbPopoverModule,NgbProgressbarModule],
  providers: [PayeesService],
})
export class PayeesModule {}
