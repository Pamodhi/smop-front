import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import{environment} from 'src/environments/environment';
import { identity, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PayeesService {
  constructor(private http:HttpClient){}

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('userId')
    }
  }

  getPayee(){
    return this.http.get(environment.apiUrl+"/PayeeManager/getPayee",this.config);
  }
  createPayee(payee :object):Observable<Object> {
    return this.http.post(environment.apiUrl+"/PayeeManager/createPayee",payee,this.config);
  }
}
