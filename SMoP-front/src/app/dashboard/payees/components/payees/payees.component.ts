import { Component, OnInit } from "@angular/core";
import { PayeesService } from "../../services/payees.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { DatePipe } from '@angular/common';
import { trim } from "jquery";

@Component({
  selector: "app-payees",
  templateUrl: "./payees.component.html",
  styleUrls: ["./payees.component.scss"],
  providers: [DatePipe]
})
export class PayeesComponent implements OnInit {
  userData = [];
  payeeData;

  payeeForm: FormGroup;
  payee: FormGroup;
  payeerClass: String = "col-xl-12";
  addPayeerForm: Boolean = true;

  myDate = new Date();
  date: String;

  constructor(private payeesServices: PayeesService, private router: Router, private datePipe: DatePipe) {
    this.date = this.datePipe.transform(this.myDate, 'yyyy/MM/dd');
  }

  formBuilder() {
    this.payeeForm = new FormGroup({
      payee_name: new FormControl(null, Validators.required),
      payee_account: new FormControl("", Validators.required),
      payee_phone: new FormControl("", Validators.required),
      payee_email: new FormControl("", Validators.required),
    });

    this.payee = new FormGroup({
      payeeName: new FormControl("",Validators.required),
    });
  }

  ngOnInit() {
    this.formBuilder();
    this.getPayees();
    this.getUsers();
    console.log(this.date)
  }

  getPayees() {
    this.payeesServices.getPayee().subscribe(
      (data) => {
        this.payeeData=data;
        console.log(data);
      },
      (err) => {}
    );
  }
  addPayeer() {
    this.payeerClass = "col-xl-7";
    this.addPayeerForm = true;
  }

  getUsers() {
    this.userData = [
      { date: "12/08/2020", account: "875264785", ses: "houserent", amount: "15,000" },
      { date: "12/09/2020", account: "875264785", ses: "houserent", amount: "15,000" },
      { date: "12/10/2020", account: "875264785", ses: "houserent", amount: "15,000" },
      { date: "12/11/2020", account: "875264785", ses: "houserent", amount: "15,000" },
      { date: "12/12/2020", account: "875264785", ses: "houserent", amount: "15,000" },
    ];
  }

  onSubmit() {
    if(this.validateForm()){
      this.addPayeerForm = false;
    this.payeerClass = "col-xl-12";
    this.payeesServices.createPayee(this.payeeForm.value).subscribe(
      (data) => {
        console.log(data);
      },
      (err) => {}
      );
    }else{
      this.payeeForm.markAllAsTouched();
    }

  }

  validateForm(){
    if (
      trim(this.payeeForm.get('payee_name').value) !== '' && this.payeeForm.get('payee_name').valid &&
      trim(this.payeeForm.get('payee_account').value) !== '' && this.payeeForm.get('payee_account').valid &&
      trim(this.payeeForm.get('payee_phone').value) !== '' && this.payeeForm.get('payee_phone').valid &&
      trim(this.payeeForm.get('payee_email').value) !== '' && this.payeeForm.get('payee_email').valid
    ){
      return true;
    }else{
      return false;
    }
  }

}
