import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamilySyncComponent } from './family-sync.component';


describe('BasicTabsPillsComponent', () => {
describe('BasicTabsPillsComponent', () => {

describe('FamilySyncComponent', () => {
  let component: FamilySyncComponent;
  let fixture: ComponentFixture<FamilySyncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamilySyncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamilySyncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
