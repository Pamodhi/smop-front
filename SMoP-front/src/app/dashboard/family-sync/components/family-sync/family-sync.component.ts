import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FamilySyncService } from '../../services/family-sync.service';

@Component({
  selector: 'app-family-sync',
  templateUrl: './family-sync.component.html',
  styleUrls: ['./family-sync.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FamilySyncComponent implements OnInit {
  public btnLoader: boolean;
  public submitLoader: boolean;

  groupData;
  groupTransactionData;
  grtransgrid;
  transData;
  transDataGrid;

  groupCreateForm: FormGroup;
  addFamilyForm: FormGroup;
  joinGropForm: FormGroup;

  constructor( private familySyncServive: FamilySyncService) {
    this.btnLoader = false;
    this.submitLoader = false;
  }

  ngOnInit() {
    this.formBuilder();
  }

  getTransData(){
   this.transData=[{ date: "12/08/2020", account: "875264785", ses: "houserent", amount: "15,000" },
    { date: "12/09/2020", account: "875264785", ses: "houserent", amount: "15,000" }]
  }


  formBuilder() {
    this.groupCreateForm = new FormGroup({
      gr_name: new FormControl("", Validators.required),
      gr_code: new FormControl("", Validators.required),
      gr_des: new FormControl("", Validators.required),
    });

    this.addFamilyForm = new FormGroup({
      gremail: new FormControl("", Validators.required),
      grid: new FormControl("", Validators.required),
      grname :new FormControl("", Validators.required),
    });

    this.joinGropForm = new FormGroup({
      group_name: new FormControl("", Validators.required),
      group_code: new FormControl("", Validators.required),
    });
  }

  onBtnLoader() {
    this.btnLoader = true;
    setTimeout(() => {
      this.btnLoader = false;
    }, 2000);
  }

  onSubmitLoader() {
    this.submitLoader = true;
    setTimeout(() => {
      this.submitLoader = false;
    }, 2000);
  }
  grant(){

  }
  addMembertoGroup(){
    this.familySyncServive.joinGroup(this.joinGropForm.value).subscribe(
      data=>{
        alert('successfully joined');
        this.joinGropForm.reset();
      },
      err=>{

      }
    );
  }

  createGroup(){
    this.familySyncServive.createGroup(this.groupCreateForm.value).subscribe(
      data=>{
        alert('successfully Group Created');
        this.groupCreateForm.reset();
      },
      err=>{

      }
    );


  }

  addGroup(){
    this.familySyncServive.addGroups(this.addFamilyForm.value).subscribe(
      data=>{
        if(data['msg']=="Success"){
          alert("successfully Group Created");
        }else{
          alert("Error, process Unsuccessful");
        }
      },
      err=>{
        console.log(err);

      }
    );
  }

  getAllTransactions(){
    this.familySyncServive.grantAccess().subscribe(
      data=>{
        this.transData=data;
      },
      err=>{
        alert("Error");
      }
    );
  }

}

/*Object.keys(formGroup.controls).forEach((key) =>
formGroup.get(key).setValue(formGroup.get(key).value.trim()));*/
