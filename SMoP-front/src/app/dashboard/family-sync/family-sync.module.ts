import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../theme/shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { NgbPopoverModule, NgbProgressbarModule,NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import {FamilySyncComponent} from './components/family-sync/family-sync.component'


@NgModule({
  declarations: [FamilySyncComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    NgbPopoverModule,
    NgbProgressbarModule,
    NgbTabsetModule
  ]
})
export class FamilySyncModule { }
