import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FamilySyncService {

  constructor(private http: HttpClient) { }

  config = {
    headers: {
      token: localStorage.getItem('token'),
      userId: localStorage.getItem('id')
    }
  }

  createGroup(group: object){
    return this.http.post(environment.apiUrl+"/FamilySync/createGroup",group,this.config);
  }

  joinGroup(group: object){
    return this.http.post(environment.apiUrl+"/FamilySync/joinGroup",group,this.config);
  }

  grantAccess(){
    return this.http.get(environment.apiUrl+"/FamilySync/getAllTransactions",this.config);
  }

  addGroups(group: object){
    return this.http.post(environment.apiUrl+"/FamilySync/sendRequest",group,this.config);
  }
  getsharedTrans(){

  }
}
