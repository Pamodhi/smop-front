import { TestBed } from '@angular/core/testing';

import { FamilySyncService } from './family-sync.service';

describe('FamilySyncService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FamilySyncService = TestBed.get(FamilySyncService);
    expect(service).toBeTruthy();
  });
});
